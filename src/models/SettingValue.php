<?php

namespace yagerguo\yii2setting\models;

use Yii;

/**
 * This is the model class for table "setting_value".
 *
 * @property integer $id
 * @property integer $categoryId
 * @property integer $groupId
 * @property string $title
 * @property string $value
 * @property string $slug
 * @property integer $type
 * @property string $desc
 * @property integer $status
 * @property integer $order
 * @property string $assist
 */
class SettingValue extends \yii\db\ActiveRecord
{
    
    const TYPE_STRING = 100;
    const TYPE_TEXT = 200;
    const TYPE_RICH = 300;

    public static function tableName()
    {
        return 'setting_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug'], 'unique'],
            [['categoryId', 'groupId', 'type', 'status', 'order'], 'integer'],
            [['value', 'desc', 'assist'], 'string'],
            [['title', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryId' => 'Category ID',
            'groupId' => 'Group ID',
            'title' => 'Title',
            'value' => 'Value',
            'slug' => 'Slug',
            'type' => 'Type',
            'desc' => 'Desc',
            'status' => 'Status',
            'order' => 'Order',
            'assist' => 'Assist',
            
            'categoryText' => '分类',
            'groupText' => '分组',
            'typeText' => '类型',
        ];
    }

    /**
     * @inheritdoc
     * @return SettingValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SettingValueQuery(get_called_class());
    }
    
    public function getCategory(){
        return $this->hasOne(SettingCategory::className(), ['id' => 'categoryId']);
    }
    
    public function getGroup(){
        return $this->hasOne(SettingGroup::className(), ['id' => 'groupId']);
    }
    
    public function getCategoryText(){
        return empty($this->category) ? '' : $this->category->name;
    }
    
    public function getGroupText(){
        return empty($this->group) ? '' : $this->group->name;
    }
    
    public static function typeData(){
        return [
            self::TYPE_STRING => '短文本',
            self::TYPE_TEXT => '长文本',
            self::TYPE_RICH => '富文本',
        ];
    }
    
    public static function dropdownTypeData(){
        return self::typeData();
    }
    
    public function getTypeText(){
        $this->type = empty($this->type) ? self::TYPE_STRING : $this->type;
        return self::typeData()[$this->type];
    }
    
}
