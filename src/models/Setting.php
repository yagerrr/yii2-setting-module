<?php

namespace yagerguo\yii2setting\models;

use yagerguo\yii2setting\models\SettingValue;

class Setting{
    
    public static function get($slug){
        $value = SettingValue::find()->andWhere(['slug' => $slug])->one();
        return empty($value) ? '' : $value->value;
    }
}