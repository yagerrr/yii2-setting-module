<?php

namespace yagerguo\yii2setting\models;

/**
 * This is the ActiveQuery class for [[SettingGroup]].
 *
 * @see SettingGroup
 */
class SettingGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SettingGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SettingGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}