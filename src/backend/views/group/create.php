<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SettingGroup */

$this->title = '添加设置分组';
$this->params['breadcrumbs'][] = ['label' => '设置分组', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
