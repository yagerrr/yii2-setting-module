<?php

namespace yagerguo\yii2setting\models;

use Yii;

/**
 * This is the model class for table "setting_group".
 *
 * @property integer $id
 * @property integer $categoryId
 * @property string $name
 * @property integer $order
 */
class SettingGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'order'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryId' => 'Category ID',
            'name' => 'Name',
            'order' => 'Order',
        ];
    }

    /**
     * @inheritdoc
     * @return SettingCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SettingCategoryQuery(get_called_class());
    }
    
    public static function dropdownData($catid){
        $items = self::find()->andWhere(['categoryId' => $catid])->orderBy('order')->all();
        $result = [];
        foreach($items as $item){
            $result[$item->id] = $item->name;
        }
        return $result;
    }
    
}
