<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SettingValue */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => '设置项', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-value-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'categoryId',
            'groupId',
            'title',
            'value:ntext',
            'slug',
            'type',
            'desc:ntext',
            'status',
            'order',
            'assist:ntext',
        ],
    ]) ?>

</div>
