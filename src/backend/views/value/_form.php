<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="setting-value-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'categoryId')->dropDownList(['' => ''] + yagerguo\yii2setting\models\SettingCategory::dropdownData()) ?>

    <?= $form->field($model, 'groupId')->dropDownList(['' => '请选择'] + yagerguo\yii2setting\models\SettingGroup::dropdownData($model->categoryId)) ?>

    <?= $form->field($model, 'type')->dropDownList(['' => '请选择'] + yagerguo\yii2setting\models\SettingValue::dropdownTypeData()) ?>
    
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'assist')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$js = <<<JS
        
jQuery(document).ready(function (){
        
    _settingvalue_cagegoryid = jQuery('#settingvalue-categoryid');
    _settingvalue_groupid    = jQuery('#settingvalue-groupid');
    
    function updateGroup(catid){
        jQuery.getJSON('get-groups?catid=' + catid, function (data){
            var html = '';
            for(var i=0; i<data.length; i++){
                html += '<option value="'+ data[i].id +'">'+ data[i].name +'</option>';
            }
            _settingvalue_groupid.html(html);
        })
    }
        
    _settingvalue_cagegoryid.on('change', function (){
        _this = $(this);
        updateGroup(_this.val())
    } )
    
});
        
JS;
$this->registerJs($js, yii\web\View::POS_END);