<?php

use yagerguo\yii2setting\models\SettingGroup;

$groups = SettingGroup::find()->andWhere(['categoryId' => $category->id])->orderBy('order')->all();
?>
<div role="tabpanel" class="tab-pane<?= $isFirstFlag ? ' active' : '' ?>" id="<?= $slug ?>">
    <?php
    $thisContent = '';
    foreach($groups as $groupOne){
        $thisContent .= $this->render('_group_content', [
            'group' => $groupOne,
            'form' => $form
        ]);
    }
    echo $thisContent;
    ?>
</div>
