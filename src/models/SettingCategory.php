<?php

namespace yagerguo\yii2setting\models;

use Yii;

/**
 * This is the model class for table "setting_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order
 */
class SettingCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order' => 'Order',
        ];
    }

    /**
     * @inheritdoc
     * @return SettingCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SettingCategoryQuery(get_called_class());
    }
    
    public static function dropdownData(){
        $cats = self::find()->orderBy('order')->all();
        $result = [];
        foreach($cats as $cat){
            $result[$cat->id] = $cat->name;
        }
        return $result;
    }
}
