<?php
use yagerguo\yii2setting\models\SettingValue;

$settings = SettingValue::find()->andWhere(['groupId' => $group->id])->indexBy('slug')->orderBy('order')->all();

if(!empty($settings)){
    echo '<h3>'. $group->name .'</h3>';
    foreach($settings as $index => $setting){
        
        $field = $form->field($setting, "[$index]value")->label($setting->title)->hint($setting->desc);
        
        switch ($setting->type){
            case SettingValue::TYPE_TEXT:
                $field = $field->textarea(['rows' => 3]);
                break;
            case SettingValue::TYPE_RICH:
                $field = $field->widget(\yii\redactor\widgets\Redactor::className());
                break;
        }
        
        echo $field;
    }
}


