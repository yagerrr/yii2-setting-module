<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_061130_setting extends Migration
{
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('setting_category', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'order' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        $this->createTable('setting_group', [
            'id' => Schema::TYPE_PK,
            'categoryId' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'order' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        $this->createTable('setting_value', [
            'id' => Schema::TYPE_PK,
            'categoryId' => Schema::TYPE_INTEGER,
            'groupId' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
            'value' => Schema::TYPE_TEXT,
            'slug' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_INTEGER,
            'desc' => Schema::TYPE_TEXT,
            'status' => Schema::TYPE_INTEGER,
            'order' => Schema::TYPE_INTEGER,
            'assist' => Schema::TYPE_TEXT,
        ], $tableOptions);
    }

    public function down()
    {
        echo "m150917_061130_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
