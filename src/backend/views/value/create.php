<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SettingValue */

$this->title = '添加设置项';
$this->params['breadcrumbs'][] = ['label' => '设置项', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-value-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
