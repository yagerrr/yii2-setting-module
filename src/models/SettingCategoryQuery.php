<?php

namespace yagerguo\yii2setting\models;

/**
 * This is the ActiveQuery class for [[SettingCategory]].
 *
 * @see SettingCategory
 */
class SettingCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SettingCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SettingCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}