<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yagerguo\yii2setting\models\SettingCategory;

$SettingCategories = SettingCategory::find()->orderBy('order')->all();

$this->title = '设置';
?>

<?php $form = ActiveForm::begin(); ?>
<div class="setting-value-index">
    
    <div class="well">
        <a href="/setting/value" class="btn btn-default">管理设置</a> <a href="/setting/category" class="btn btn-default">管理分类</a> <a href="/setting/group" class="btn btn-default">管理分组</a>
    </div>

    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <?php
        $tabContent = '';
        $isFirstFlag = true;
        foreach($SettingCategories as $SettingCategoryOne){
            $thisSlug = 'sc'. $SettingCategoryOne->id;
        ?>
        <li role="presentation" class="<?= $isFirstFlag ? 'active' : '' ?>"><a href="#<?= $thisSlug ?>" aria-controls="<?= $thisSlug ?>" role="tab" data-toggle="tab"><?= $SettingCategoryOne->name ?></a></li>
        <?php
            $tabContent .= $this->render('_category_content', [
                'category' => $SettingCategoryOne,
                'slug' => $thisSlug,
                'isFirstFlag' => $isFirstFlag,
                'form' => $form,
            ]);
            $isFirstFlag = false;
        }
        ?>
    </ul>

    <div class="tab-content" style="max-width: 660px; min-height: 60px;">
        <?= $tabContent ?>
    </div>
    
    <div class="form-group" style="clear: both;">
        <?= Html::submitButton('保存设置', ['class' => 'btn btn-primary']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>
