<?php

namespace yagerguo\yii2setting;

class SettingModule extends \yii\base\Module
{
    public $controllerNamespace = 'yagerguo\yii2setting\controllers\backend';
    public $backendViewPath;

    public function init()
    {
        parent::init();
        
        if(empty($this->backendViewPath)){
            $this->backendViewPath = __DIR__ . '/backend/views/';
        }
    }
}
